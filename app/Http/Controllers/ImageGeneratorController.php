<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class ImageGeneratorController extends Controller{

  public function __construct()
  {  }

  public function generateImg(Request $request){
    ini_set('max_execution_time', 3600);

    $sourceFolderPath = '../../public/img_src/';
    $resultFolderPath = '../../public/img_temp/';

    $algo = $request->input('algoritma', "img_quilting");
    $sourceFileName = $request->input('sourceFile', 'potongansadum0.jpg');
    $resultFileName = "genImg" . str_replace('.', '', $sourceFileName) . "_" . $request->input("fileName") . str_random(3);

    $sourceFile = $sourceFolderPath . $sourceFileName;
    $resultFile = $resultFolderPath . $resultFileName . '.jpg';

    switch ($algo) {
      case 'img_quilting':
        $treshold = $request->input('treshold_similarity', 80);
        $treshold = $treshold / 100;

        $command = "cd matlab_file/img_quilting/ && matlab -wait -nosplash -nodesktop -nodisplay -r \"img_quilting2('"
          .$sourceFile."', '"
          .$resultFile."', "
          .$treshold."); exit;\"";

        exec($command, $execResult, $retval);

        return response()->json(array('error' => false,
          'message' => 'Generate image success',
          'filename' => $resultFileName,
          'exec_result' => url("public/img_temp") . "/" . $resultFileName . '.jpg',
          'algoritma' => $algo),
          200);

        break;
      case 'img_warping':
        $intp_method = $request->input('interpolar_method', 'invdist');
        $intp_radius = $request->input('interpolar_radius', 5);
        $intp_power = $request->input('interpolar_power', 2);

        $command = "cd matlab_file/img_warping/ && matlab -wait -nosplash -nodesktop -nodisplay -r "
                    ."\"tpsWarpDemo('".$sourceFile."', '"
                    .$resultFile."', "
                    ."'map.mat', "
                    ."'tpsDemoLandmark.mat', '"
                    .$intp_method."', "
                    .$intp_radius.", "
                    .$intp_power
                    ."); exit;\"";

        exec($command, $execResult, $retval);

        return response()->json(array('error' => false,
          'message' => 'Generate image success',
          'filename' => $resultFileName,
          'exec_result' => url("public/img_temp") . "/" . $resultFileName . '.jpg',
          'algoritma' => $algo),
          200);

        break;
      case 'non_parametric_sample':
        $block_size = $request->input('block_size', 10);

        $command = "cd matlab_file/non_parametric/ && matlab -wait -nosplash -nodesktop -nodisplay -r "
                    ."\"nps_main('".$sourceFile."', '"
                    .$resultFile."', "
                    .$block_size
                    ."); exit;\"";

        exec($command, $execResult, $retval);

        return response()->json(array('error' => false,
          'message' => 'Generate image success',
          'filename' => $resultFileName,
          'exec_result' => url("public/img_temp") . "/" . $resultFileName . '.jpg',
          'algoritma' => $algo),
          200);

        break;
      default:
          return response()->json(array('error' => true,
          'message' => 'Undefined algoritma [' . $algo . ']'),
          200);
        break;
    }
  }

}
