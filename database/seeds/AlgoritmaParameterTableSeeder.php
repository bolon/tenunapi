<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\AlgoritmaParameter;

class AlgoritmaParameterTableSeeder extends Seeder
{
  public function run(){
    AlgoritmaParameter::create([
        'id_algoritma' => 1,
        'type_algoritma' => 'Float',
        'min_val' => '50',
        'max_val' => '80',
        'nama_parameter' => 'treshold_similarity'
    ]);

    AlgoritmaParameter::create([
        'id_algoritma' => 2,
        'type_algoritma' => 'String',
        'nama_parameter' => 'interpolar_method'
    ]);

    AlgoritmaParameter::create([
        'id_algoritma' => 2,
        'type_algoritma' => 'int',
        'min_val' => '5',
        'max_val' => '10',
        'nama_parameter' => 'interpolar_radius'
    ]);

    AlgoritmaParameter::create([
        'id_algoritma' => 2,
        'type_algoritma' => 'int',
        'min_val' => '2',
        'max_val' => '4',
        'nama_parameter' => 'interpolar_power'
    ]);
  }
}
