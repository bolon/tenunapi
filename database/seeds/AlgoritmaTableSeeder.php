<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Algoritma;

class AlgoritmaTableSeeder extends Seeder
{
  public function run(){
    Algoritma::create([
        'nama_algoritma' => 'img_quilting',
        'description' => 'Test'
    ]);

    Algoritma::create([
        'nama_algoritma' => 'img_warping',
        'description' => 'Soemthing'
    ]);
  }
}
